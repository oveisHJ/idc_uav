clear all
close all
im=imread('D:\Image Processing\2017 data collection\Leonard\Mosaicked\06252017_100FtLeonardMosaicked.tif');
r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);%fourthBand=im(:,:,4);

pic=cat(3,r,g,b);

% pic=imrotate(pic,0.8);
pic=imcrop(pic);
imshow(pic)
imwrite(pic,'D:\Image Processing\2017 data collection\Leonard\Mosaicked\06252017_100FtLeonardMosaicked_Cropped.tif')