clear all
close all

mm = '07';
dd = '29';
loc = 'Amenia';
% loc = 'Colfax';
% loc = 'Leonard';
% loc = 'Hunter';

path = strcat('E:\Image Processing\2016 data collection\Small UAS\',mm,'-',dd,'-2016\Cropped\',mm,dd,'2016_100Ft',loc,'.tif');
% path = strcat('E:\Image Processing\2017 data collection\',loc,'\Calibrated\',mm,dd,'2017_100Ft',loc,'Calibrated.tif');

im=imread(path);
lab = rgb2lab(im);
a= lab(:,:,2);
if min(min(a))<0    
    a = a + abs(min(min(a)));
end
a = a/max(max(a));
imtool(a(1:1000,1:1000))
a=medfilt2(a,[3 3]);

threshold = graythresh(a);
mask = imbinarize(a,threshold);

mask = ~mask;
imtool(mask(1:1000,1:1000))
%%
% % % 
% % % b = lab(:,:,3);
% % % if min(min(b))<0
% % %     b = b + abs(min(min(b)));
% % % end
% % % b = b/max(max(b));
% % % threshold = graythresh(b);
% % % mask = imbinarize(b,threshold);
% imtool(b)
%%
r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
% GB = g-b;
% GB = 2*g-b-r;

% imtool(GB)
% k = [0,-1,0;-1,5,-1;0,-1,0];
% im_sharpen = imfilter(GB,k);
% mask_threshold = graythresh(im_sharpen);
% mask = imbinarize(im_sharpen,mask_threshold);
% imtool(mask)
% mask = imopen(mask, strel('disk',3));
r(mask==0)=0;g(mask==0)=0;b(mask==0)=0;
im_masked = cat(3,r,g,b);
% imtool(im_sharpen)
imtool(im_masked)

%%
hsv=rgb2hsv(im);
h=hsv(:,:,1);s=hsv(:,:,2);v=hsv(:,:,3);
% imtool(hsv)
DarkGreenColorIndex=(((h*360-60)/60)+(1-s)+(1-v))/3;
DarkGreenColorIndex(mask==0)=0;
DarkGreenColorIndex(DarkGreenColorIndex<0.001 | DarkGreenColorIndex>0.7 | h==0)=0;
%%
imtool(DarkGreenColorIndex)
% output_path = strcat('E:\Image Processing\2016 data collection\Small UAS\',mm,'-',dd,'-2016\DGCI\',mm,dd,'2016_100Ft',loc,'DGCI_2.tif');
output_path = strcat('E:\Image Processing\2017 data collection\',loc,'\DGCI\',mm,dd,'2017_100Ft',loc,'_2.tif');
imwrite(DarkGreenColorIndex,output_path);
imtool(a(1:500,1:500))