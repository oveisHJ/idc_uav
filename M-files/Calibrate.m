clear all
close all
im=imread('D:\Image Processing\2017 data collection\Leonard\Mosaicked\07242017_100FtLeonardMosaicked_Cropped.tif');

% im200=imread('D:\Image Processing\2016 data collection\Small UAS\07-29-2016\Calibrated\07292016_200FtColfaxCal.tif');
r=im(:,:,1);g=im(:,:,2);b=im(:,:,3);
r=im2double(r);g=im2double(g);b=im2double(b);
pic=cat(3,r,g,b);

color=false(500);
while size(color,1)<600
   color=imcrop(pic);
   red=color(:,:,1);green=color(:,:,2);blue=color(:,:,3);
   red=sum(red(:))/numel(red)
   green=sum(green(:))/numel(green)
   blue=sum(blue(:))/numel(blue)
end

%%
coefR=1.4182; interceptR=-0.4268;
R=coefR*r+interceptR;

coefG=1.5787;   interceptG=-0.5628;
G=coefG*g+interceptG;

coefB=1.3858;  intercpetB=-0.3702;
B=coefB*b+intercpetB;
R=im2uint8(R);G=im2uint8(G);B=im2uint8(B);
newPic=cat(3,R,G,B);
imtool(newPic)