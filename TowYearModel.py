import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import numpy as np

df=pd.read_csv(r'C:\Users\Oveis\Desktop\idc_uav\ML.csv')
'''
df.groupby('Location').apply(pd.Series.isnull).sum()
group=df.groupby('Location').mean()
df.isnull().sum()
'''
#TimeIt
#%timeit df.isnull().values.any()

imputer=Imputer()
#df.iloc[:,[0,1,2,3,9]]=imputer.fit_transform(df.iloc[:,[0,1,2,3,9]])
df[df.YLD3==np.nan]=0
df.loc[df.Location=='Leonard',['23 Jun', '1 Jul', '22 Jul', '29 Jul']]=imputer.fit_transform(df.loc[df.Location=='Leonard',['23 Jun', '1 Jul', '22 Jul', '29 Jul']])
#df.loc[df.Location=='Leonard',['25-Jun', '3-Jul', '8-Jul', '14-Jul', '24-Jul']]=imputer.fit_transform(df.loc[df.Location=='Leonard'['25-Jun', '3-Jul', '8-Jul', '14-Jul', '24-Jul']])

df.dropna(inplace=True)
Locations=list(df.Location.unique())

df2=pd.read_csv(r'Test2017.csv')
df2.dropna(inplace=True)
X1=df.iloc[:,:4]
y1=df['YLD3']
X2=df2.iloc[:,[0,1,3,4]]
X2.columns= X1.columns
y2=df2['YLD3']
X=pd.concat([X1,X2],axis=0)
y=pd.concat([y1,y2], axis=0)
def RF(min_samples=5):
    
    X_train, X_test, y_train, y_test = train_test_split(X1, y1, test_size=0.25, random_state=13)
    reg=RandomForestRegressor(random_state=13, n_estimators=100, min_samples_leaf=min_samples)
    CV=cross_val_score(reg, X_train, y_train, cv=5)
    print(CV)
    print(CV.mean())
    print('='*80)
    reg.fit(X_train, y_train)
    params=[{'n_estimators':[50,70,100],
            'min_samples_leaf':[3,5,7,10,15,25]}]
    GSCV=GridSearchCV(reg,param_grid=params,cv=5,scoring='r2')
    GSCV.fit(X_train, y_train)
    print("the best estimator is {}".format(GSCV.best_estimator_))
    print(GSCV.best_score_)
    reg.fit(X_train, y_train)
    y_pred=reg.predict(X_test)
    print('score for train is:', reg.score(X_train, y_train))
    print('score for test  is:', reg.score(X_test, y_test))
    #print('score for test  is:', reg.score(X1, y1))
    print('RMSE is:',np.sqrt(mean_squared_error(y_test, y_pred)))
    import matplotlib.pyplot as plt
    #plt.scatter(y_test, y_pred)
    y2.hist()
    plt.xlabel(r'Yield bu/acre')
    plt.ylabel('frequency')
    #plt.savefig(r'y2_hist.tif')
    #print(len(X2.index))
    #print(len(y2_predict.index))
    plt.show()
    #X_try=pd.DataFrame([0.219796,0.208407,0.239436,0.09056]).transpose()
    #print(reg.predict(X_try))
    plt.scatter(y_test, y_pred)
    plt.show()
    
RF(15)