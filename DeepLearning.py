# Importing libraries
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error
#importing the dataset
df=pd.read_csv('All_Indices_0701.csv')
#df=df.drop(df.iloc[:,14:],axis=1)
#extracting X and y from dataset
first=3
last=6
X=df.iloc[:,[3,4,5]].values
y=np.array(df.loc[:,'SPAD'])
#splitting dataset to training and test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test= train_test_split(X, y, test_size=0.3, random_state=42)

'''import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import SGD

model=Sequential()
model.add(Dense(1,input_shape=(3,)))
model.add(Activation('relu'))
sgd=SGD(0.01)
model.compile(loss='mse',optimizer=sgd,metrics=['mse'])
H=model.fit(X_train,y_train,nb_epoch=100)

''' '''*****************************************************************************************
******************************************************************************************'''
'''print('*'*80)
#Importing the Keras libraries and packages

#Initialising the ANN
ann=Sequential()

#Adding the input layer and the first hidden layer
ann.add(Dense(output_dim=2, init='uniform',activation='relu',input_dim=3))
#Adding the second hidden layer
ann.add(Dense(output_dim=4, init='uniform',activation='linear'))
sgd=SGD(0.01)
#adding the output layer
ann.add(Dense(output_dim=1, init='uniform',activation='linear'))
#Making prediction and evaluating the model
ann.compile(optimizer=sgd, metrics=['mse'],loss='mean_squared_error')
H=ann.fit(X_train, y_train, batch_size=5, nb_epoch=100)
y_pred=ann.predict(X_test)

print(mean_squared_error(y_test,y_pred))
import matplotlib.pyplot as plt
plt.plot(H.history['mean_squared_error'])'''
print('*'*80)

'''*****************************************************************************************
******************************************************************************************'''
#DataCamp model
from keras.layers import Dense
from keras.models import Sequential
from keras.callbacks import EarlyStopping
#from keras.optimizers import SGD
early_stopping_monitor=EarlyStopping(patience=12)
#==============================================================================
# from sklearn.preprocessing import StandardScaler
# sc=StandardScaler()
# X=sc.fit_transform(X)
# y=sc.fit_transform(y)
#==============================================================================
n_cols=X.shape[1]
input_shape=(n_cols,)
node=64
activation='relu'
def get_new_model(input_shape=input_shape):
    model=Sequential()
    model.add(Dense(node,activation=activation,input_shape=input_shape))
    model.add(Dense(node,activation=activation))
    model.add(Dense(node,activation=activation))
    model.add(Dense(node,activation=activation))
    model.add(Dense(node,activation=activation))
    #model.add(Dense(node,activation=activation))
    #model.add(Dense(node,activation=activation))
    #model.add(Dense(node,activation=activation))
    #model.add(Dense(node,activation=activation))
    #model.add(Dense(node,activation=activation))

    
    model.add(Dense(1))    
    return(model)

lr_to_test=[0.005]
for lr in lr_to_test:
    print('\n\nTesting model with learning rate: %f\n'%lr )
    print('*'*80)
    model=get_new_model()
    my_optimizer='adam'
    model.compile(optimizer=my_optimizer, loss='mean_squared_error')
    H=model.fit(X_train, y_train, batch_size=20, epochs=1000, callbacks=[early_stopping_monitor])
    print('min of history is {}'.format(np.min(H.history['loss'])))

#import matplotlib.pyplot as plt
#plt.plot(H.history['loss'])
y_pred=model.predict(X_test)
print('MSE for test set is {}'.format(mean_squared_error(y_test,y_pred)))

#==============================================================================
#from keras.models import load_model
#model.save('node64Relu6LayersAdamBatch5epoch1000_best_01July.h5')
#==============================================================================

df=pd.read_csv('All_Indices_0715.csv')
X=df.iloc[:,[3,4,5]].values
y=df.loc[:,'SPAD'].values
#model=load_model('node100ReluAdamBatch5epoch300_best.h5')
y_pred=model.predict(X)
print('MSE for X is {}'.format(mean_squared_error(y,y_pred)))
import matplotlib.pyplot as plt
plt.scatter(y_pred,y)
plt.show()
