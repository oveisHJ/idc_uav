# Importing libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import GridSearchCV


#poly=PolynomialFeatures(interaction_only=True)

#importing the dataset
df01=pd.read_csv('All_Indices_0701.csv')
df01=df01[df01['Plot Number']!=103]
#df=df.drop(df.iloc[:,14:],axis=1)
#extracting X and y from dataset
first=0
last=3
X01=df01.iloc[:,first:last]
y01=df01.loc[:,'SPAD']
x_DGCI01=df01.iloc[:,8:9]
df15=pd.read_csv('All_Indices_0715.csv')
#for July 15 file several columns need to be dropped
df15=df15.drop(df15.iloc[:,14:],axis=1)
#extracting X and y from dataset
X15=df15.iloc[:,first:last]
y15=df15.loc[:,'SPAD']
x_DGCI15=df15.iloc[:,8:9]
#ExR=X.iloc[:,0]*1.3-X.iloc[:,1]
y=pd.concat([y01,y15])
X=pd.concat([X01,X15])

#X=pd.concat([X,ExR],axis=1,join_axes=[X.index])
#X=poly.fit_transform(X)
#X['ExR']=X.iloc[:,0]*1.3-X.iloc[:,1]
#X['GB']=X.iloc[:,1]-X.iloc[:,2]

#X=pd.read_csv('DoubleValues.csv')
x_DGCI=pd.concat([x_DGCI01,x_DGCI15])
#X['DGCI']=x_DGCI
#splitting dataset to training and test set
#from sklearn.model_selection import train_test_split
#X_train, X_test, y_train, y_test= train_test_split(X, y, test_size=0.3, random_state=42)
#X_train_DGCI, X_test_DGCI, y_train_DGCI, y_test_DGCI= train_test_split(x_DGCI, y, test_size=0.3, random_state=42)

#DGCI Single Linear Regression
regTitle="DGCI"
from sklearn.linear_model import LinearRegression
singleLinearD=LinearRegression()
singleLinearD.fit(x_DGCI,y)
y_pred=singleLinearD.predict(x_DGCI)
#print("R Square for {} is {}".format(regTitle, singleLinearD.score(X_test_DGCI,y_test_DGCI)))
#print("mean square error for {} is {}".format(regTitle, mean_squared_error(y,y_pred)))
cv=cross_val_score(singleLinearD, x_DGCI, y, cv=10, scoring='neg_mean_squared_error')
#cv=cross_val_score(singleLinearD, x_DGCI, y, cv=10, scoring='r2')
cv=np.sqrt(-cv)
#cvLin=cv
print(cv)
print("average of cross_validation is {}".format(cv.mean()))
print("Standard deviation of cross_validation is {}".format(cv.std()))
#building simple linear regression model
regTitle="Simple Linear Regression"
from sklearn.linear_model import LinearRegression
singleLinear=LinearRegression()
singleLinear.fit(X,y)
y_pred=singleLinear.predict(X)
#print("R Square for {} is {}".format(regTitle, singleLinear.score(X,y)))
#print("mean square error for {} is {}".format(regTitle, mean_squared_error(y,y_pred)))
print('*'*80)
#building random forest
regTitle='Random Forest'
from sklearn.ensemble import RandomForestRegressor
# =============================================================================
# randomForest=RandomForestRegressor(random_state=13,n_estimators=5,max_depth=9, min_samples_leaf=5) 
# parameters= [{'n_estimators':[20,40,50,100,200,300,400],
#              'max_depth':[2,3,4,5,6,7,8,12],
#              'min_samples_leaf':[2,4,6,8,10]}
#             ]
# gridSearch=GridSearchCV(randomForest,param_grid=parameters,scoring='r2', cv=10)
# gridSearch.fit(X,y)
# print(gridSearch.score)
# print(gridSearch.best_estimator_)
# print(gridSearch.best_params_)
# =============================================================================
randomForest=RandomForestRegressor(random_state=13,n_estimators=100, max_depth=6, min_samples_leaf=6) 
cv=cross_val_score(randomForest, X, y, cv=10, scoring='neg_mean_squared_error')
#cv=cross_val_score(randomForest, X, y, cv=10, scoring='r2')
cv=np.sqrt(-cv)
#cvRF=cv
print(cv)
randomForest.fit(X, y)
y_pred=randomForest.predict(X)
#print("R Square for {} is {}".format(regTitle, randomForest.score(X,y)))
#print("mean square error for {} is {}".format(regTitle, mean_squared_error(y,y_pred)))
print("average of cross_validation is {}".format(cv.mean()))
print("Standard deviation of cross_validation is {}".format(cv.std()))
#======================================================================================
#======================================================================================
#======================================================================================
#Multiple linear regression assumption check
#1 outcome should have a linear relationship with all predictors
#y_ass=singleLinear.predict(X)
#plt.scatter(X.iloc[:,0],y_ass,c='r')
#plt.scatter(X.iloc[:,1],y_ass,c='g')
#plt.scatter(X.iloc[:,2],y_ass,c='b')
#plt.show()
#2 residuals should be normally distributed
#y_residual=y_ass-y
#plt.hist(y_residual)
#plt.show()
"""
plt.scatter(y,singleLinear.predict(X.iloc[:,:last]),c='r')
plt.scatter(y,randomForest.predict(X.iloc[:,:last]),c='b')

plt.scatter(y,singleLinearD.predict(x_DGCI),c='g')

plt.legend()
plt.show()
"""
'''
regTitle='AdaBoost'
from sklearn.ensemble import AdaBoostRegressor
reg=AdaBoostRegressor(DecisionTreeRegressor(random_state=45,max_depth=4),n_estimators=100,learning_rate=0.1,loss='square', random_state=45)
reg.fit(X_train, y_train)
y_predada=reg.predict(X_test)
print("R Square for {} is {}".format(regTitle, reg.score(X_test,y_test)))
print("mean square error for {} is {}".format(regTitle, mean_squared_error(y_test,y_predada)))
'''
#======================================================================================
#======================================================================================
#======================================================================================