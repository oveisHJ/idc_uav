<<<<<<< HEAD
import pandas as pd 
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt

df=pd.read_csv('Test2017.csv')
df.dropna(inplace=True)


if __name__=="__main__":

    #print(df.Variety.value_counts())
    print(df.Variety.nunique())
    df['AUC']=(df.iloc[:,1]+df.iloc[:,0])*4 + (df.iloc[:,2]+df.iloc[:,1])*2.5 + (df.iloc[:,3]+df.iloc[:,2])*3 + (df.iloc[:,4]+df.iloc[:,3])*5

    sc=MinMaxScaler()
    sc.fit(df[['AUC','IDC1']])
    df[['AUC','IDC1']]=sc.transform(df[['AUC','IDC1']])
    agg_df=df.groupby('Variety', as_index=False).mean()    
    h2_auc=(agg_df['AUC'].describe()['std'])**2/(df['AUC'].describe()['std'])**2
    h2_idc=(agg_df['IDC1'].describe()['std'])**2/(df['IDC1'].describe()['std'])**2
    print('Heritability of AUC is: {}'.format(h2_auc))
    print('Heritability of IDC is: {}'.format(h2_idc))
        
    agg_df.plot(x='Variety',y=['IDC1','AUC'], kind='bar',subplots=True)
    
    auc=df[['AUC','Variety']]
    idc=df[['IDC1','Variety']]
    auc=auc.pivot(columns='Variety', values='AUC')
    idc=idc.pivot(columns='Variety', values='IDC1')
    auc.plot(kind='box')
    plt.xticks(rotation=90)
    idc.plot(kind='box')
    plt.xticks(rotation=90)
    plt.show()
=======
import pandas as pd 
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt

df=pd.read_csv('Test2017.csv')
df.dropna(inplace=True)


if __name__=="__main__":

    #print(df.Variety.value_counts())
    print(df.Variety.nunique())
    df['AUC']=(df.iloc[:,1]+df.iloc[:,0])*4 + (df.iloc[:,2]+df.iloc[:,1])*2.5 + (df.iloc[:,3]+df.iloc[:,2])*3 + (df.iloc[:,4]+df.iloc[:,3])*5

    sc=MinMaxScaler()
    sc.fit(df[['AUC','IDC1']])
    df[['AUC','IDC1']]=sc.transform(df[['AUC','IDC1']])
    agg_df=df.groupby('Variety', as_index=False).mean()    
    h2_auc=(agg_df['AUC'].describe()['std'])**2/(df['AUC'].describe()['std'])**2
    h2_idc=(agg_df['IDC1'].describe()['std'])**2/(df['IDC1'].describe()['std'])**2
    print('Heritability of AUC is: {}'.format(h2_auc))
    print('Heritability of IDC is: {}'.format(h2_idc))
        
    agg_df.plot(x='Variety',y=['IDC1','AUC'], kind='bar',subplots=True)
    
    auc=df[['AUC','Variety']]
    idc=df[['IDC1','Variety']]
    auc=auc.pivot(columns='Variety', values='AUC')
    idc=idc.pivot(columns='Variety', values='IDC1')
    auc.plot(kind='box')
    plt.xticks(rotation=90)
    idc.plot(kind='box')
    plt.xticks(rotation=90)
    plt.show()
>>>>>>> master
